import os
import tempfile
from random import random

import pytest
from tango.server import Device, command
from tango.test_context import DeviceTestContext

from ska_pact_tango.consumer import Consumer
from ska_pact_tango.provider import Interaction, Provider
from ska_pact_tango.verifier import verifier


class ProviderDevice(Device):
    """Sample Provider Tango device"""

    def init_device(self, *args, **kwargs):  # pylint: disable=W0235
        """Init the device"""
        super().init_device(*args, **kwargs)

    @command(dtype_in=int, dtype_out=int)
    def add_int_to_five(self, num):
        """Add a random number to the passed in number

        :param num: A number
        :type num: int
        :return: A number plus a randum number
        :rtype: float
        """
        return 5 + num

    @command(dtype_in=None, dtype_out=float)
    def no_arg_command(self):
        """Test command with no argument"""
        return random()

    @command(dtype_in=int, dtype_out=float)
    def with_arg_command(self, num):
        """Test command with argument"""
        return num + random()


def test_verify():

    pact = Consumer("test/consumer/1", consumer_cls=ProviderDevice).has_pact_with(
        providers=[
            Provider("test/nodb/providerdevice").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .and_given("with_arg_command has ran", "with_arg_command", 5)
                .and_given("no_arg_command has ran", "no_arg_command")
                .and_given("Only description")
                .upon_receiving("A request to run add_int_to_five")
                .with_request("command", "add_int_to_five", 5)
                .will_respond_with(int, 10)
            )
        ]
    )

    with DeviceTestContext(ProviderDevice, process=True) as proxy:

        @verifier("./tests/pact_test_case.json", "A request to run add_int_to_five")
        def verify_provider(*args, **kwargs):
            interaction = kwargs["interaction"]
            device_name = kwargs["device_name"]

            # In a normal test against running devices you would do:
            # proxy = tango.DeviceProxy(device_name)
            # Since we're testing the verifier in a DeviceTestContext we use the supplied proxy

            interaction.setup_provider(proxy)
            interaction.verify_interaction(proxy)

        verify_provider()


def test_non_existant_command():

    with DeviceTestContext(ProviderDevice, process=True) as proxy:

        with tempfile.TemporaryDirectory() as tmpdir:
            test_file_name = "test_pact_file.json"

            pact = Consumer("test/consumer/1", consumer_cls=ProviderDevice).has_pact_with(
                providers=[
                    Provider("test/nodb/providerdevice").add_interaction(
                        Interaction()
                        .given("The provider is in Init State", "CommandDoesNotExist")
                        .and_given("Only description")
                        .upon_receiving("A request to run add_int_to_five")
                        .with_request("command", "add_int_to_five", 5)
                        .will_respond_with(int, 10)
                    )
                ],
                pact_dir=tmpdir,
                pact_file_name=test_file_name,
            )

            pact.write_pact()

            @verifier(os.path.join(tmpdir, test_file_name), "A request to run add_int_to_five")
            def verify_provider(*args, **kwargs):
                interaction = kwargs["interaction"]
                device_name = kwargs["device_name"]

                with pytest.raises(AttributeError) as exc:  # CommandDoesNotExist
                    interaction.setup_provider(proxy)
                interaction.verify_interaction(proxy)

            verify_provider()


def test_incorrect_param():

    with DeviceTestContext(ProviderDevice, process=True) as proxy:

        with tempfile.TemporaryDirectory() as tmpdir:
            test_file_name = "test_pact_file.json"

            pact = Consumer("test/consumer/1", consumer_cls=ProviderDevice).has_pact_with(
                providers=[
                    Provider("test/nodb/providerdevice").add_interaction(
                        Interaction()
                        .given(
                            "The provider is in Init State",
                            "add_int_to_five",
                            "A string should be an int",
                        )
                        .and_given("Only description")
                        .upon_receiving("A request to run add_int_to_five")
                        .with_request("command", "add_int_to_five", 5)
                        .will_respond_with(int, 10)
                    )
                ],
                pact_dir=tmpdir,
                pact_file_name=test_file_name,
            )

            pact.write_pact()

            @verifier(os.path.join(tmpdir, test_file_name), "A request to run add_int_to_five")
            def verify_provider(*args, **kwargs):
                interaction = kwargs["interaction"]
                device_name = kwargs["device_name"]

                with pytest.raises(TypeError):
                    interaction.setup_provider(proxy)
                interaction.verify_interaction(proxy)

            verify_provider()
