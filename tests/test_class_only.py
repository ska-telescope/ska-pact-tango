import os
import tempfile

import pytest
import tango

from ska_pact_tango.consumer import Consumer
from ska_pact_tango.pact import Pact
from ska_pact_tango.provider import Interaction, Provider


class ConsumerDevice:
    """Dummy Consumer device"""


class PowerSupplyConsumer:
    """Used to test pact without Tango"""

    def __init__(self, dev_name):
        self.dev_name = dev_name
        self.dp = tango.DeviceProxy(self.dev_name)

    def get_voltage_attr(self):
        """read attribute"""
        return self.dp.read_attribute("voltage")

    def get_current(self):
        """read attribute short"""
        return self.dp.current

    def ramp_command(self, target_current):
        """run command"""
        return self.dp.ramp(target_current)


def test_non_tango_device_attribute():
    """Test that read_attribute works outside of Tango device"""
    expected = 1.0

    pact = Consumer("consumer", consumer_cls=PowerSupplyConsumer).has_pact_with(
        providers=[
            Provider("test/powersupply/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request to run read_attribute of voltage")
                .with_request("read_attribute", "voltage")
                .will_respond_with(float, expected)
            )
        ]
    )

    with pact:
        consumer = PowerSupplyConsumer("test/powersupply/1")
        assert consumer.get_voltage_attr() == expected


def test_non_tango_device_attr():
    """Test that short form read attribute works outside of Tango device"""
    expected = 1.0

    pact = Consumer("consumer", consumer_cls=PowerSupplyConsumer).has_pact_with(
        providers=[
            Provider("test/powersupply/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request to run read_attribute of voltage")
                .with_request("attribute", "current")
                .will_respond_with(float, expected)
            )
        ]
    )

    with pact:
        consumer = PowerSupplyConsumer("test/powersupply/1")
        assert consumer.get_current() == expected


def test_non_tango_device_run_command():
    """Test that running a command works outside of Tango device"""
    expected = 1.0

    pact = Consumer("consumer", consumer_cls=PowerSupplyConsumer).has_pact_with(
        providers=[
            Provider("test/powersupply/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request to run command ramp")
                .with_request("command", "ramp", 5.0)
                .will_respond_with(float, expected)
            )
        ]
    )

    with pact:
        consumer = PowerSupplyConsumer("test/powersupply/1")
        assert consumer.ramp_command(5.0) == expected


def test_provider_name_mismatch():
    """Test that there are interactions for a provider"""
    expected = 1.0

    pact = Consumer("consumer", consumer_cls=PowerSupplyConsumer).has_pact_with(
        providers=[
            Provider("test/powersupply/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request to run read_attribute of voltage")
                .with_request("read_attribute", "voltage")
                .will_respond_with(float, expected)
            )
        ]
    )

    with pytest.raises(ValueError) as exc_raised:
        with pact:
            consumer = PowerSupplyConsumer("some_other_name")
            assert consumer.get_voltage_attr() == expected
    assert exc_raised.value.args[0] == (
        "There are no interactions for provider [some_other_name], " "available providers: ['test/powersupply/1']"
    )


def test_dict_transform():
    """Test Pact to and from dict"""
    pact = Consumer("consumer", consumer_cls=PowerSupplyConsumer).has_pact_with(
        providers=[
            Provider("test/powersupply/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request to run read_attribute of voltage")
                .with_request("read_attribute", "voltage")
                .will_respond_with(float, 1.0)
            ),
            Provider("test/powersupply/2").add_interaction(
                Interaction()
                .given("The provider is in Off State", "Off")
                .upon_receiving("A second request to run read_attribute of voltage")
                .with_request("read_attribute", "voltage")
                .will_respond_with(float, 2.0)
            ),
        ]
    )
    pact_dict = pact.to_dict()
    assert Pact.from_dict(pact_dict) == pact


def test_file_parse():
    """Test that the pact is parsed as expected"""
    with tempfile.TemporaryDirectory() as tmpdir:
        test_file_name = "test_pact_file.json"

        pact = Consumer("consumer", consumer_cls=ConsumerDevice).has_pact_with(
            providers=[
                Provider("provider").add_interaction(
                    Interaction()
                    .given("The provider is in Init State", "Init")
                    .upon_receiving("A request to run add_int_to_random command")
                    .with_request("command", "add_int_to_random", 3)
                    .will_respond_with("float", 1.0)
                )
            ],
            pact_dir=tmpdir,
            pact_file_name=test_file_name,
        )

        assert pact.pact_dir == tmpdir
        assert pact.pact_file_name == test_file_name
        assert pact.pact_json_file_path == os.path.join(tmpdir, test_file_name)

        pact.write_pact()

        new_pact = Pact.from_file(os.path.join(tmpdir, test_file_name))
        assert pact == new_pact
