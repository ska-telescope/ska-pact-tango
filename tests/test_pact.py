# pylint: disable=C0330

import json
import os
import tempfile

import pytest
from tango import DeviceAttribute

from ska_pact_tango.consumer import Consumer
from ska_pact_tango.provider import Interaction, Provider


class ConsumerDevice:
    """Dummy Consumer device"""


def test_pact_creation():
    pact = Consumer("test/consumer/1").has_pact_with(
        providers=[
            Provider("test/device/1")
            .add_interaction(
                Interaction()
                .given("Provider test/device/1 in some state", "command1", "param 1", "param 2")
                .and_given("Provider test/device/1 in some other state", "command2", "param 3", "param 4")
                .upon_receiving("A request to run add_int_to_random1 command")
                .with_request("command", "add_int_to_random1", 3)
                .will_respond_with("float", 1.0)
            )
            .add_interaction(
                Interaction()
                .given("Provider test/device/1 in some second state", "command3", "param 5", "param 6")
                .and_given(
                    "Provider test/device/1 in some other second state",
                    "command4",
                    "param 7",
                    "param 8",
                )
                .upon_receiving("A request to run add_int_to_random2 command")
                .with_request("command", "add_int_to_random2", 3)
                .will_respond_with("float", 1.0)
            ),
            Provider("test/device/2").add_interaction(
                Interaction()
                .given("Provider test/device/2 in some state", "command4", "param 1", "param 2")
                .and_given("Provider in some other state", "command5", "param 3", "param 4")
                .upon_receiving("A request to run add_int_to_random3 command")
                .with_request("command", "add_int_to_random3", 3)
                .will_respond_with("float", 1.0)
            ),
        ],
        pact_dir=".",
        pact_file_name="some_file",
    )

    assert pact.to_dict() == {
        "consumer": {"name": "test/consumer/1"},
        "providers": [
            {
                "provider": {
                    "name": "test/device/1",
                    "interactions": [
                        {
                            "description": "A request to run add_int_to_random1 command",
                            "provider_states": [
                                {
                                    "description": "Provider test/device/1 in some state",
                                    "request": {
                                        "type": "command",
                                        "name": "command1",
                                        "args": ["param 1", "param 2"],
                                    },
                                },
                                {
                                    "description": "Provider test/device/1 in some other state",
                                    "request": {
                                        "type": "command",
                                        "name": "command2",
                                        "args": ["param 3", "param 4"],
                                    },
                                },
                            ],
                            "request": {
                                "type": "command",
                                "name": "add_int_to_random1",
                                "args": [3],
                            },
                            "response": {"response": "1.0", "response_type": "float"},
                        },
                        {
                            "description": "A request to run add_int_to_random2 command",
                            "provider_states": [
                                {
                                    "description": "Provider test/device/1 in some second state",
                                    "request": {
                                        "type": "command",
                                        "name": "command3",
                                        "args": ["param 5", "param 6"],
                                    },
                                },
                                {
                                    "description": "Provider test/device/1 in some other second state",
                                    "request": {
                                        "type": "command",
                                        "name": "command4",
                                        "args": ["param 7", "param 8"],
                                    },
                                },
                            ],
                            "request": {
                                "type": "command",
                                "name": "add_int_to_random2",
                                "args": [3],
                            },
                            "response": {"response": "1.0", "response_type": "float"},
                        },
                    ],
                }
            },
            {
                "provider": {
                    "name": "test/device/2",
                    "interactions": [
                        {
                            "description": "A request to run add_int_to_random3 command",
                            "provider_states": [
                                {
                                    "description": "Provider test/device/2 in some state",
                                    "request": {
                                        "type": "command",
                                        "name": "command4",
                                        "args": ["param 1", "param 2"],
                                    },
                                },
                                {
                                    "description": "Provider in some other state",
                                    "request": {
                                        "type": "command",
                                        "name": "command5",
                                        "args": ["param 3", "param 4"],
                                    },
                                },
                            ],
                            "request": {
                                "type": "command",
                                "name": "add_int_to_random3",
                                "args": [3],
                            },
                            "response": {"response": "1.0", "response_type": "float"},
                        }
                    ],
                }
            },
        ],
        "metadata": {"specification": {"version": "1.0"}},
    }


def test_attribute():
    """Test that an attribute is stored correctly"""
    device_attribute_response = DeviceAttribute()
    device_attribute_response.value = 99.99

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A read attribute request for the attribute random_float")
                .with_request("read_attribute", "random_float")
                .will_respond_with(DeviceAttribute, device_attribute_response)
            )
        ]
    )

    assert pact.to_dict() == {
        "consumer": {"name": "test/consumer/1"},
        "providers": [
            {
                "provider": {
                    "name": "test/provider/1",
                    "interactions": [
                        {
                            "description": "A read attribute request for the attribute random_float",
                            "provider_states": [
                                {
                                    "description": "The provider is in Init State",
                                    "request": {"type": "command", "name": "Init", "args": []},
                                }
                            ],
                            "request": {
                                "type": "read_attribute",
                                "name": "random_float",
                                "args": [],
                            },
                            "response": {
                                "response": "DeviceAttribute[\ndata_format = tango._tango.AttrDataFormat.FMT_UNKNOWN\n      dim_x = 0\n      dim_y = 0\n       name = 'Name not set'\n    nb_read = 0\n nb_written = 0\n    quality = tango._tango.AttrQuality.ATTR_INVALID\nr_dimension = AttributeDimension(dim_x = 0, dim_y = 0)\n       time = TimeVal(tv_nsec = 0, tv_sec = 0, tv_usec = 0)\n      value = 99.99\n    w_dim_x = 0\n    w_dim_y = 0\nw_dimension = AttributeDimension(dim_x = 0, dim_y = 0)]\n",
                                "response_type": "<class 'tango._tango.DeviceAttribute'>",
                            },
                        }
                    ],
                }
            }
        ],
        "metadata": {"specification": {"version": "1.0"}},
    }


def test_file_write_default_paths():
    """Test that an attribute is stored correctly"""
    pact = Consumer("consumer", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("provider").add_interaction(
                Interaction()
                .given("The provider is in Init State", "A given parameter")
                .upon_receiving("A request to run add_int_to_random command")
                .with_request("command", "add_int_to_random", 3)
                .will_respond_with("float", 1.0)
            )
        ]
    )
    assert pact.pact_dir == os.getcwd()
    assert pact.pact_file_name == "consumer-pact.json"
    assert pact.pact_json_file_path == os.path.join(pact.pact_dir, pact.pact_file_name)


def test_file_write():
    """Test that the pact is written to file"""
    with tempfile.TemporaryDirectory() as tmpdir:
        test_file_name = "test_pact_file.json"

        pact = Consumer("consumer", consumer_cls=ConsumerDevice).has_pact_with(
            providers=[
                Provider("provider").add_interaction(
                    Interaction()
                    .given("The provider is in Init State", "A given parameter")
                    .upon_receiving("A request to run add_int_to_random command")
                    .with_request("command", "add_int_to_random", 3)
                    .will_respond_with("float", 1.0)
                )
            ],
            pact_dir=tmpdir,
            pact_file_name=test_file_name,
        )

        assert pact.pact_dir == tmpdir
        assert pact.pact_file_name == test_file_name
        assert pact.pact_json_file_path == os.path.join(tmpdir, test_file_name)

        pact.write_pact()

        with open(pact.pact_json_file_path, "r") as written_file:
            pact_data = json.load(written_file)
            assert pact_data == {
                "consumer": {"name": "consumer"},
                "providers": [
                    {
                        "provider": {
                            "name": "provider",
                            "interactions": [
                                {
                                    "description": "A request to run add_int_to_random command",
                                    "provider_states": [
                                        {
                                            "description": "The provider is in Init State",
                                            "request": {
                                                "type": "command",
                                                "name": "A given parameter",
                                                "args": [],
                                            },
                                        }
                                    ],
                                    "request": {
                                        "type": "command",
                                        "name": "add_int_to_random",
                                        "args": [3],
                                    },
                                    "response": {"response": "1.0", "response_type": "float"},
                                }
                            ],
                        }
                    }
                ],
                "metadata": {"specification": {"version": "1.0"}},
            }


@pytest.mark.parametrize(
    "with_request_args,expected",
    [
        (
            # provider_device.random_float = 5.0
            ["attribute", "random_float", 5.0],
            {"type": "attribute", "name": "random_float", "args": [5.0]},
        ),
        (
            # provider_device.write_attribute(“random_float”, 5.0)
            ["method", "write_attribute", "random_float", 5.0],
            {"type": "method", "name": "write_attribute", "args": ["random_float", 5.0]},
        ),
        (
            # provider_device.write_attribute(“random_float”, 5.0)
            ["write_attribute", "random_float", 5.0],
            {"type": "method", "name": "write_attribute", "args": ["random_float", 5.0]},
        ),
        (
            # provider_device.random_float
            ["attribute", "random_float"],
            {"type": "attribute", "name": "random_float", "args": []},
        ),
        (
            # provider_device.read_attribute(“random_float”)
            ["read_attribute", "random_float"],
            {"type": "read_attribute", "name": "random_float", "args": []},
        ),
        (
            # provider_device.SomeCommand()
            ["command", "SomeCommand"],
            {"type": "command", "name": "SomeCommand", "args": []},
        ),
        (
            # provider_device.SomeCommand(1.0)
            ["command", "SomeCommand", 1.0],
            {"type": "command", "name": "SomeCommand", "args": [1.0]},
        ),
        (
            # provider_device.command_inout(“SomeCommand”)
            ["method", "command_inout", "SomeCommand"],
            {"type": "method", "name": "command_inout", "args": ["SomeCommand"]},
        ),
        (
            # provider_device.command_inout(“SomeCommand”)
            ["command_inout", "SomeCommand"],
            {"type": "method", "name": "command_inout", "args": ["SomeCommand"]},
        ),
        (
            # provider_device.command_inout(“SomeCommand”, 1.0)
            ["method", "command_inout", "SomeCommand", 1.0],
            {"type": "method", "name": "command_inout", "args": ["SomeCommand", 1.0]},
        ),
        (
            # provider_device.command_inout(“SomeCommand”, 1.0)
            ["command_inout", "SomeCommand", 1.0],
            {"type": "method", "name": "command_inout", "args": ["SomeCommand", 1.0]},
        ),
    ],
)
def test_with_request_permutations(with_request_args, expected):
    """Test the with_request options"""

    pact = Consumer("consumer", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("provider").add_interaction(
                Interaction()
                .given("The provider is in Init State", "A given parameter")
                .upon_receiving("A request to run add_int_to_random command")
                .with_request(*with_request_args)
                .will_respond_with("float", 1.0)
            )
        ]
    )

    request = pact.to_dict()["providers"][0]["provider"]["interactions"][0]["request"]
    assert request["type"] == expected["type"]
    assert request["name"] == expected["name"]
    assert request["args"] == expected["args"]
