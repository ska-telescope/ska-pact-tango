#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
from setuptools import setup

with open("README.md") as readme_file:
    readme = readme_file.read()

setup(
    name="ska_pact_tango",
    description="Square Kilometre Array PACT testing utility for Tango",
    long_description=readme + "\n\n",
    long_description_content_type="text/markdown",
    author="Team Karoo",
    author_email="cam+karoo@ska.ac.za",
    url="https://gitlab.com/ska-telescope/ska-pact-tango",
    packages=[
        "ska_pact_tango",
    ],
    package_dir={"": "src"},
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Software Development :: Testing",
    ],
    keywords="ska Pact Tango test",
    python_requires=">=3.6",
    test_suite="tests",
    install_requires=["pytango == 9.3.3"],
    use_katversion=True,
    tests_require=["tox"],
    entry_points={"console_scripts": ["pact-tango-verify = ska_pact_tango.pact_tango_verify:main"]},
)
