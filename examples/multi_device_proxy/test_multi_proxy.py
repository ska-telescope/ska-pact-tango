from tango.test_context import MultiDeviceTestContext, DeviceTestContext
from tango import DeviceAttribute


from multi_provider_consumer import MultiConsumerDevice
from ska_pact_tango.consumer import Consumer
from ska_pact_tango.provider import Provider, Interaction

devices_info = [
    {"class": MultiConsumerDevice, "devices": (
        {"name": "test/consumer/1", "properties": {}},)}
]


def test_consumer_to_provider_attribute_read():
    """Test the attribute read"""
    device_attribute_response1 = DeviceAttribute()
    device_attribute_response1.value = 1.1

    device_attribute_response2 = DeviceAttribute()
    device_attribute_response2.value = 2.2

    pact = Consumer("test/consumer/1", consumer_cls=MultiConsumerDevice).has_pact_with(
        providers=[
            Provider("test/multiprovider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A read attribute request for the attribute random_float")
                .with_request("attribute", "random_float")
                .will_respond_with(DeviceAttribute, device_attribute_response1)
            ),
            Provider("test/multiprovider/2").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A read attribute request for the attribute random_float")
                .with_request("attribute", "random_float")
                .will_respond_with(DeviceAttribute, device_attribute_response2)
            ),
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/1")
            assert int(consumer.read_providers_random_float() * 10) == 33

    with pact:
        with DeviceTestContext(MultiConsumerDevice, process=True) as consumer:
            assert int(consumer.read_providers_random_float() * 10) == 33

    # Check that mocks don't swop around
    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/1")
            assert consumer.read_provider1_random_float() == 1.1

    with pact:
        with DeviceTestContext(MultiConsumerDevice, process=True) as consumer:
            assert consumer.read_provider1_random_float() == 1.1

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/1")
            assert consumer.read_provider2_random_float() == 2.2

    with pact:
        with DeviceTestContext(MultiConsumerDevice, process=True) as consumer:
            assert consumer.read_provider2_random_float() == 2.2
