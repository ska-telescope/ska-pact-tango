import tango

from ska_pact_tango.verifier import verifier

# The pact definition
# pact = Consumer("test/consumer/1", consumer_cls=ProviderDevice).has_pact_with(
#         providers=[
#             Provider("test/multiprovider/1").add_interaction(
#                 Interaction()
#                 .given("The provider is in Init State", "Init")
#                 .and_given("with_arg_command has ran", "with_arg_command", 5)
#                 .and_given("no_arg_command has ran", "no_arg_command")
#                 .and_given("Only description")
#                 .upon_receiving("A request to run add_int_to_five")
#                 .with_request("command", "add_int_to_five", 5)
#                 .will_respond_with(int, 10)
#             )
#         ]
#     )


@verifier("./examples/multi_device_proxy/verifier_test.json", "A request to run add_int_to_five")
def test_verify_provider(*args, **kwargs):  # pylint: disable=W0613
    """Test the Pact file"""
    interaction = kwargs["interaction"]
    device_name = kwargs["device_name"]

    proxy = tango.DeviceProxy(device_name)

    # You can use the proxy to set up the provider as needed before checking the Pact contract

    # Run the commands as defined in the Pact file prior to verifying the interaction
    # In this case it would be:
    #   proxy.Init()
    #   proxy.with_arg_command(5)
    #   proxy.no_arg_command()
    interaction.setup_provider(proxy)

    # Check the interaction `with_request` against `will_respond_with`.
    # Execute proxy.add_int_to_five(5) and make sure you get 10 back
    interaction.verify_interaction(proxy)

    # If you want to do specialized test you can execute the interaction and check the
    # result yourself.
    interaction_result = interaction.execute_request(interaction.request, proxy)
    assert interaction_result == 10
