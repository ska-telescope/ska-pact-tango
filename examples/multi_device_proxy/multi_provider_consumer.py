import os

from random import random

import tango

from tango import Database, DbDevInfo
from tango.server import Device, attribute, command


class MultiConsumerDevice(Device):
    """Sample Consumer Tango device"""

    def init_device(self, *args, **kwargs):
        """Init the device"""
        super().init_device(*args, **kwargs)
        self.provider_device = tango.DeviceProxy("test/multiprovider/1")  # pylint: disable=W0201
        self.provider_device2 = tango.DeviceProxy("test/multiprovider/2")  # pylint: disable=W0201

    @attribute(
        dtype="double",
    )
    def random_float(self):
        """Return a random number

        :return: Random number
        :rtype: float
        """
        return random()

    @command(dtype_in=None, dtype_out=float)
    def read_provider1_random_float(self):
        """ provider_device.random_float """
        return self.provider_device.random_float.value

    @command(dtype_in=None, dtype_out=float)
    def read_provider2_random_float(self):
        """ provider_device.random_float """
        return self.provider_device2.random_float.value

    @command(dtype_in=None, dtype_out=float)
    def read_providers_random_float(self):
        """ provider_device.random_float """
        return self.provider_device.random_float.value + self.provider_device2.random_float.value

    @command(dtype_in=None, dtype_out=None)
    def case_attr_set(self):
        """ provider_device.random_float """
        self.provider_device.random_float = 5.0
        self.provider_device2.random_float = 6.0

    @command(dtype_in=None, dtype_out=None)
    def case_write_attr(self):
        """ provider_device.write_attribute("random_float", 5.0) """
        self.provider_device.write_attribute("random_float", 5.0)
        self.provider_device2.write_attribute("random_float", 6.0)

    @command(dtype_in=None, dtype_out=float)
    def case_read_attr(self):
        """ provider_device.random_float """
        return self.provider_device.random_float + self.provider_device2.random_float

    @command(dtype_in=None, dtype_out=float)
    def case_read_attribute(self):
        """ provider_device.read_attribute("random_float") """
        return (
            self.provider_device.read_attribute("random_float") +
            self.provider_device2.read_attribute("random_float")
        )

    @command(dtype_in=None, dtype_out=float)
    def case_run_command(self):
        """ provider_device.SomeCommand() """
        return self.provider_device.no_arg_command() + self.provider_device2.no_arg_command()

    @command(dtype_in=None, dtype_out=float)
    def case_run_command_arg(self):
        """ provider_device.SomeCommand(1.0) """
        return self.provider_device.with_arg_command(1) + self.provider_device2.with_arg_command(2)

    @command(dtype_in=None, dtype_out=float)
    def case_command_inout(self):
        """ provider_device.command_inout("SomeCommand") """
        return (
            self.provider_device.command_inout("no_arg_command") +
            self.provider_device2.command_inout("no_arg_command")
        )

    @command(dtype_in=None, dtype_out=float)
    def case_command_inout_arg(self):
        """ provider_device.command_inout("SomeCommand", 1.0) """
        return (
            self.provider_device.command_inout("with_arg_command", 1.0) +
            self.provider_device2.command_inout("with_arg_command", 2.0)
        )


if __name__ == "__main__":
    db = Database()
    test_device = DbDevInfo()
    if "DEVICE_NAME" in os.environ:
        # DEVICE_NAME should be in the format domain/family/member
        test_device.name = os.environ["DEVICE_NAME"]
    else:
        # fall back to default name
        test_device.name = "test/multiconsumer/1"
    test_device._class = "MultiConsumerDevice"  # pylint: disable=W0212
    test_device.server = "MultiConsumerDevice/test"
    db.add_server(test_device.server, test_device, with_dserver=True)
    MultiConsumerDevice.run_server()
