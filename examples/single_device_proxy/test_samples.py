from tango.test_context import MultiDeviceTestContext, DeviceTestContext
from tango import DeviceAttribute


from consumer_device import ConsumerDevice
from ska_pact_tango.consumer import Consumer
from ska_pact_tango.provider import Provider, Interaction


devices_info = [
    {"class": ConsumerDevice, "devices": ({"name": "test/consumer/2", "properties": {}},)}
]


def test_consumer_to_provider_attribute_read():
    """Test the attribute read"""
    device_attribute_response = DeviceAttribute()
    device_attribute_response.value = 99.99

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("read_attribute", "random_float")
                .will_respond_with(DeviceAttribute, device_attribute_response)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.read_provider_random_float() == device_attribute_response.value

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.read_provider_random_float() == device_attribute_response.value


def test_consumer_to_provider_command_exec():
    """Test a command exec"""
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("command", "add_int_to_random", 3)
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.run_provider_add_int(1) == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.run_provider_add_int(1) == expected


def test_case_attr_set():
    """ provider_device.random_float = 5.0 """

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("attribute", "random_float", 5.0)
                .will_respond_with("NoneType", None)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            consumer.case_attr_set()

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            consumer.case_attr_set()


def test_case_write_attr():
    """ provider_device.write_attribute("random_float", 5.0) """

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("method", "write_attribute", "random_float", 5.0)
                .will_respond_with("NoneType", None)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            consumer.case_write_attr()

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            consumer.case_write_attr()


def test_case_write_attr_two():
    """ provider_device.write_attribute("random_float", 5.0) """

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("write_attribute", "random_float", 5.0)
                .will_respond_with("NoneType", None)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            consumer.case_write_attr()

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            consumer.case_write_attr()


def test_case_read_attr():
    """ provider_device.random_float """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("attribute", "random_float")
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_read_attr() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_read_attr() == expected


def test_case_read_attribute():
    """ provider_device.read_attribute("random_float") """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("read_attribute", "random_float")
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_read_attribute() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_read_attribute() == expected


def test_case_run_command():
    """ provider_device.SomeCommand() """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("command", "no_arg_command")
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_run_command() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_run_command() == expected


def test_case_run_command_arg():
    """ provider_device.SomeCommand(1.0) """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("command", "with_arg_command", 1.0)
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_run_command_arg() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_run_command_arg() == expected


def test_case_command_inout():
    """ provider_device.command_inout("SomeCommand") """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("method", "command_inout", "no_arg_command")
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_command_inout() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_command_inout() == expected


def test_case_command_inout_two():
    """ provider_device.command_inout("SomeCommand") """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("command_inout", "no_arg_command")
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_command_inout() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_command_inout() == expected


def test_case_command_inout_arg():
    """ provider_device.command_inout("SomeCommand", 1.0) """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("method", "command_inout", "with_arg_command", 1.0)
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_command_inout_arg() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_command_inout_arg() == expected


def test_case_command_inout_arg_two():
    """ provider_device.command_inout("SomeCommand", 1.0) """
    expected = 5.0

    pact = Consumer("test/consumer/1", consumer_cls=ConsumerDevice).has_pact_with(
        providers=[
            Provider("test/provider/1").add_interaction(
                Interaction()
                .given("The provider is in Init State", "Init")
                .upon_receiving("A request")
                .with_request("command_inout", "with_arg_command", 1.0)
                .will_respond_with("float", expected)
            )
        ]
    )

    with pact:
        with MultiDeviceTestContext(devices_info, process=True) as context:
            consumer = context.get_device("test/consumer/2")
            assert consumer.case_command_inout_arg() == expected

    with pact:
        with DeviceTestContext(ConsumerDevice, process=True) as consumer:
            assert consumer.case_command_inout_arg() == expected
