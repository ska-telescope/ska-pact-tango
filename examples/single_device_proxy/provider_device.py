import os
from random import random
from tango import Database, DbDevInfo
from tango.server import Device, attribute, command


class ProviderDevice(Device):
    """Sample Provider Tango device"""

    def init_device(self, *args, **kwargs):  # pylint: disable=W0235
        """Init the device"""
        super().init_device(*args, **kwargs)

    @attribute(dtype="double")
    def random_float(self):
        """Return a random number

        :return: Random number
        :rtype: float
        """
        return random()

    @command(dtype_in=int, dtype_out=float)
    def add_int_to_random(self, num):
        """Add a random number to the passed in number

        :param num: A number
        :type num: int
        :return: A number plus a randum number
        :rtype: float
        """
        return random() + num

    @command(dtype_in=None, dtype_out=float)
    def no_arg_command(self):
        """Test command with no argument"""
        return random()

    @command(dtype_in=int, dtype_out=float)
    def with_arg_command(self, num):
        """Test command with argument"""
        return num + random()


if __name__ == "__main__":
    db = Database()
    test_device = DbDevInfo()
    if "DEVICE_NAME" in os.environ:
        # DEVICE_NAME should be in the format domain/family/member
        test_device.name = os.environ["DEVICE_NAME"]
    else:
        # fall back to default name
        test_device.name = "test/provider/1"
    test_device._class = "ProviderDevice"  # pylint: disable=W0212
    test_device.server = "ProviderDevice/test"
    db.add_server(test_device.server, test_device, with_dserver=True)
    ProviderDevice.run_server()
