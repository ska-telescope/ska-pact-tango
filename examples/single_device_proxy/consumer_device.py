import os

from random import random

import tango

from tango import Database, DbDevInfo
from tango.server import Device, attribute, command


class ConsumerDevice(Device):
    """Sample Consumer Tango device"""

    def init_device(self, *args, **kwargs):
        """Init the device"""
        super().init_device(*args, **kwargs)
        self.provider_device = tango.DeviceProxy("test/provider/1")  # pylint: disable=W0201

    @attribute(
        dtype="double",
    )
    def random_float(self):
        """Return a random number

        :return: Random number
        :rtype: float
        """
        return random()

    @command(dtype_in=None, dtype_out=float)
    def read_provider_random_float(self):
        """Read a the random_float on the provider device

        :return: Random number
        :rtype: float
        """
        return self.provider_device.read_attribute("random_float").value

    @command(dtype_in=int, dtype_out=float)
    def run_provider_add_int(self, num):
        """Pass a number to the provider device that will add a random number to it.

        :param num: Number
        :type num: int
        :return: Random number added to passed in number
        :rtype: float
        """
        res = self.provider_device.add_int_to_random(num)
        return res

    @command(dtype_in=int, dtype_out=int)
    def echo_int(self, num):
        """Echo back the number passes in

        :param num: A number
        :type num: int
        :return: The same number
        :rtype: int
        """
        return num

    @command(dtype_in=None, dtype_out=None)
    def case_attr_set(self):
        """ provider_device.random_float = 5.0 """
        self.provider_device.random_float = 5.0

    @command(dtype_in=None, dtype_out=None)
    def case_write_attr(self):
        """ provider_device.write_attribute("random_float", 5.0) """
        self.provider_device.write_attribute("random_float", 5.0)

    @command(dtype_in=None, dtype_out=float)
    def case_read_attr(self):
        """ provider_device.random_float """
        return self.provider_device.random_float

    @command(dtype_in=None, dtype_out=float)
    def case_read_attribute(self):
        """ provider_device.read_attribute("random_float") """
        return self.provider_device.read_attribute("random_float")

    @command(dtype_in=None, dtype_out=float)
    def case_run_command(self):
        """ provider_device.SomeCommand() """
        return self.provider_device.no_arg_command()

    @command(dtype_in=None, dtype_out=float)
    def case_run_command_arg(self):
        """ provider_device.SomeCommand(1.0) """
        return self.provider_device.with_arg_command(1)

    @command(dtype_in=None, dtype_out=float)
    def case_command_inout(self):
        """ provider_device.command_inout("SomeCommand") """
        return self.provider_device.command_inout("no_arg_command")

    @command(dtype_in=None, dtype_out=float)
    def case_command_inout_arg(self):
        """ provider_device.command_inout("SomeCommand", 1.0) """
        return self.provider_device.command_inout("with_arg_command", 1.0)


if __name__ == "__main__":
    db = Database()
    test_device = DbDevInfo()
    if "DEVICE_NAME" in os.environ:
        # DEVICE_NAME should be in the format domain/family/member
        test_device.name = os.environ["DEVICE_NAME"]
    else:
        # fall back to default name
        test_device.name = "test/consumer/1"
    test_device._class = "ConsumerDevice"  # pylint: disable=W0212
    test_device.server = "ConsumerDevice/test"
    db.add_server(test_device.server, test_device, with_dserver=True)
    ConsumerDevice.run_server()
