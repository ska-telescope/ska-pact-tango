SKA PACT testing for Tango
==========================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-pact-tango/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-pact-tango/en/latest/?badge=latest)


This repository contains a Python implementation for [Pact](http://pact.io/), adapted to test the interactions between Tango devices. Pact is a specification for
Consumer Driven Contracts Testing. For further information about Pact project, contracts testing, pros and cons and
useful resources please refer to the [Pact website](http://pact.io/).

## Requirements

The system used for development needs to have Python 3 and `pip` installed.

## Install

### From source

- Clone the repo

```bash
git clone git@gitlab.com:ska-telescope/ska-pact-tango.git
```

- Install requirements

```bash
 python3 -m pip install -r requirements.txt
```

- Install the package

```bash
 python3 -m pip install .
```

### From the SKAO PyPI

```bash
 python3 -m pip install ska-pact-tango --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple
```

## Testing

- Install the test requirements

```bash
 python3 -m pip install -r requirements-test.txt
```

- Run the tests

```bash
tox
```

- Lint

```bash
tox -e lint
```

## Writing documentation

The documentation generator for this project is derived from SKA's [SKA Developer Portal repository](https://github.com/ska-telescope/developer.skatelescope.org)

The documentation can be edited under `./docs/src`

### Build the documentation

- Install the test requirements

```bash
 python3 -m pip install -r requirements-test.txt
```

- Build docs

 ```bash
tox -e docs
```

The documentation can then be consulted by opening the file `./docs/build/html/index.html`