##########
Change Log
##########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[Unreleased]
************

v1.0.1
******

* Moving artefacts to a new repository https://artefact.skao.int/.

v1.0.0
******

* This is the initial release of ska-pact-tango.