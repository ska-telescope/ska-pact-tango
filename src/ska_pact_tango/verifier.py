from functools import wraps

from ska_pact_tango.pact import Pact


def verifier(pact_file_path, description):
    """A convenience decorator that adds the interaction and device_name to kwargs.
       The relevant interaction is looked up from the description.


    :param pact_file_path: The path to the Pact file
    :type pact_file_path: str
    :param description: The interaction description, as defined in the Pact file
    :type description: str
    """

    def testfunc_decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            pact = Pact.from_file(pact_file_path)
            interaction = None
            device_name = ""
            for provider in pact.providers:
                interaction = provider.get_interaction_from_description(description)
                device_name = provider.device_name
                if interaction:
                    break
            assert interaction, f"Could not find an interaction description" f" that matches {description}"
            kwargs["interaction"] = interaction
            kwargs["device_name"] = device_name
            return func(*args, **kwargs)

        return wrapped_function

    return testfunc_decorator
