import argparse

import tango

from ska_pact_tango.pact import Pact


def verify_pact(opts: argparse.Namespace):
    """Run through all the interactions in the Pact file and verify them.

    :param opts: The parsed options
    :type opts: argparse.Namespace
    """
    verbose = opts.verbose
    if verbose:
        print(f"Loading Pact from {opts.pact_file_path.name}")
    pact = Pact.from_file(opts.pact_file_path.name)
    if verbose:
        print("Done")
    for provider in pact.providers:
        device_name = provider.device_name
        if verbose:
            print(f"Checking against provider [{device_name}]")
        proxy = tango.DeviceProxy(device_name)
        for interaction in provider.interactions:
            if verbose:
                print(f"Checking interaction [{interaction.interaction_description}]")
            interaction.setup_provider(proxy)
            interaction.verify_interaction(proxy)


def get_argparser() -> argparse.ArgumentParser:
    """Create the argument parser

    :return: The argument parser
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description="Verify a Pact file against the provider")
    parser.add_argument("pact_file_path", type=argparse.FileType("r"))
    parser.add_argument("-v", "--verbose", help="Print test output", action="store_true")
    return parser


def main():
    """Script entrypoint"""
    arg_parser = get_argparser()
    opts = arg_parser.parse_args()
    verify_pact(opts)


if __name__ == "__main__":
    main()
