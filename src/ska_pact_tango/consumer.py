from ska_pact_tango.pact import Pact
from ska_pact_tango.provider import Provider


class Consumer:
    """
    A Pact consumer.

    Use this class to describe the consumer executing commands on the provider and
    then use `has_pact_with` to create a contract with a specific provider.
    """

    def __init__(self, name, consumer_cls=None):
        """
        Constructor for the Consumer class.

        :param name: The name of this Consumer.
        :type name: str
        :type consumer_cls: The Tango device class
        :type class: class
        """
        self.name = name
        self.consumer_cls = consumer_cls

    def has_pact_with(self, providers=[], pact_dir="", pact_file_name=""):  # pylint: disable=W0102
        """
        Create a contract between the provider and this consumer.

        :param providers: A list of providers that this contract has
        :type provider: List[pact.Provider]
        :param pact_dir: Directory where the resulting pact files will be
            written. Defaults to the current directory.
        :type pact_dir: str
        :param pact_file_name: The name of the pact file for this interaction.
            Defaults to <consumer_name>-pact.json
        :type pact_file_name: str
        :rtype: pact.Pact
        """
        for provider in providers:
            if not isinstance(provider, Provider):
                raise ValueError("provider must be an instance of the Provider class.")

        return Pact(
            providers=providers,
            consumer_name=self.name,
            consumer_class=self.consumer_cls,
            pact_dir=pact_dir,
            pact_file_name=pact_file_name,
        )
