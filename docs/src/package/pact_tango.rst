***************
Pact with Tango
***************

This document explains the reasoning behind Pact testing and how it applies to Tango devices.
The devices used are based on those defined in the `multi_device_proxy` folder examples.
See `examples/multi_device_proxy` folder for device implementations as well as test examples.

Terminology
###########

First, some Pact terms used below.

- Pact
    Defines the interactions between the consumers and provider(s).

- Consumer
    The Tango device under test that itself creates a proxy to another Tango device (provider) and interacts with it.
    I.e the consumer is a client of the provider. The consumer (client) could create multiple proxies to several providers (servers).


- Provider
    The Tango device that the consumer interacts with.
    There could be several providers that the consumer connects to.

- Interaction
    A request-response pair.
    The request from the consumer and the response from the provider.

- Mocked Provider
    The actual running provider is replaced with a mocked provider and it's behavior is defined by interactions.

- Pact file (contract)
    The serialized JSON file that describes a Pact between consumer and provider(s). This is the `contract` as
    defined in the Pact literature.

- Pact verification
    The process of ensuring the validity of a Pact file by running and verifying the interactions against the actual provider.

- Provider state
    The state that the provider should be in prior to doing the pact verification. This is not limited to the State attribute
    of the provider, but could include attribute values.



Pact test process
#################

Normal operation
****************

In our example below the MultiConsumerDevice creates two proxies to MultiProviderDeviceA and MultiProviderDeviceB, respectively.
The consumer then gets a command `read_providers_random_float`. The Consumer then reads the attribute `random_float` from each proxy.
The numbers are added together and returned to the user as the result of `read_providers_random_float`.

.. image:: seq_normal.png
  :alt: Normal operation

Consumer side test
******************

Here we see that both providers have now been replaced by the mocked provider.
The result of reading `random_float` from the providers are now determined by the Pact file.
The Consumer is oblivious to the fact that the real providers don't exist.
Now the result of command `read_providers_random_float` on the consumer can be verified since
the response from the providers are known.

.. image:: seq_consumer.png
  :alt: Normal operation

Provider side test
******************

To ensure that the provider responds as the pact expects we need to verify the interactions.

ProviderA

.. image:: seq_provider_a.png
  :alt: Normal operation

ProviderB

.. image:: seq_provider_b.png
  :alt: Normal operation