.. doctest-skip-all
.. _package-guide:

**************
Python Modules
**************

This section details the public API for using the Pact testing package.


Public API Documentation
````````````````````````

.. automodule:: ska_pact_tango.pact
  :members:
  :undoc-members:
  :show-inheritance:

.. automodule:: ska_pact_tango.consumer
  :members:
  :undoc-members:
  :show-inheritance:

.. automodule:: ska_pact_tango.provider
  :members:
  :undoc-members:
  :show-inheritance:

.. automodule:: ska_pact_tango.verifier
  :members:
  :undoc-members:
  :show-inheritance:

